class DeclareConstraint:
    """
    This class represents declare constraints. Every constraint can have a type, one or two actions and a boolean value 
    showing if it is negative.
    """

    def __init__(self, constraint_type, action_a, action_b=None, is_negative=False):
        self.type = constraint_type
        self.action_a = action_a
        self.action_b = action_b
        self.is_negative = is_negative

    def size(self): #unary constraint: size 2, non-negative binary constraint: size 3, negative binary contraint: size 4
        n = 2
        if self.action_b is not None:
            n += 1
        if self.is_negative:
            n += 1
        return n

    def __str__(self):
        if self.action_b is None:
            return f"Constraint [type={self.type}, A={self.action_a}]"
        if self.is_negative:
            return f"Constraint [type= NOT-{self.type}, A={self.action_a}, B={self.action_b}]"
        return f"Constraint [type={self.type}, A={self.action_a}, B={self.action_b}]"

    
    '''
    for evaluation
    (check whether a discovered constraint matches the expected constraint -> gold standard)
    '''
    def equals_gs(self, gsc):
        return (
            self.type() == gsc.type()
            and self.are_equal(self.action_a(), gsc.action_a())
            and self.are_equal(self.action_b(), gsc.action_b())
        )

    @staticmethod
    def are_equal(a, gsa):
        return gsa.action_str().strip().lower() == a.action_str().strip().lower()
