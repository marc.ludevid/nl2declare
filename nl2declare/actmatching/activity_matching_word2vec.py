from gensim.models import KeyedVectors
import numpy as np
import gensim.downloader as api

"""
    This class uses Word2Vec to calculate the similarity between word phrases.
"""

path = api.load("word2vec-google-news-300", return_path=True)
word2vec_model_path = path
word2vec_model = KeyedVectors.load_word2vec_format(word2vec_model_path, binary=True, limit=500000)


def get_most_similar_activity_without_common_words(activity: str, activities: list[str]):
    """
    get_most_similar_activity_without_common_words: This function search for a given string the most similar 
    object out of a given list of strings. For this it checks for every combination of the input string 
    and a string from the list if they have at least one word in common. If not the similarity is 0. If 
    they have all words in common the similarity is 1. Else we calculate the Word2Vec similarity value for
    both strings but with their common words removed.

    Parameters:
        - activity (str): A string value
        - activities (list[str]): A list of string values

    Returns:
        - most simililar item to the activity out of the list objects
    """
    similarities = []

    for act in activities:
        if have_common_word(activity,act):
            #remove common words in the strings
            new_start_word, new_second_word = remove_common_words(activity,act)

            new_start_word_list = new_start_word.split()
            new_second_word_list = new_second_word.split()

            if new_start_word == "" and new_second_word == "":
                similarities.append(1)
            elif new_start_word == "" or new_second_word == "":
                similarities.append(0)
            elif len(new_start_word_list) > 1 or len(new_second_word_list) > 1: #e.g. target label from log: creation of claim, match with create claim
                similarities.append(0)
            else:
                #calculate similarity 
                similarity_value = word2vec_model.similarity(new_start_word, new_second_word)
                similarities.append(similarity_value)

        else:
            similarities.append(0)

    return activities[np.argmax(similarities)], max(similarities)


def have_common_word(stringOne, stringTwo):
    """
    have_common_word: This function checks for the input strings if they have at least one word in common.

    Parameters:
        - stringOne (str): A string value
        - stringTwo (str): A string value

    Returns:
        - boolean value with True = both strings hava at least one common word.
    """
    wordsOne = set(stringOne.split())
    wordsTwo = set(stringTwo.split())
    common_words = wordsOne.intersection(wordsTwo)

    # Return True if there is at least one common word, else False
    return bool(common_words)


def remove_common_words(stringOne, stringTwo):
    """
    remove_common_words: This function removes the common words of both input strings. The changed
    strings are then returned as tuple

    Parameters:
        - stringOne (str): A string value
        - stringTwo (str): A string value

    Returns:
        - None (if no common words are detected) or a tuple of the edited strings.
    """
    wordsOne = set(stringOne.split())
    wordsTwo = set(stringTwo.split())
    common_words = wordsOne.intersection(wordsTwo)

    # If there are common words, remove them and return the strings
    if common_words:
        new_string_one = ' '.join(word for word in wordsOne if word not in common_words)
        new_string_two = ' '.join(word for word in wordsTwo if word not in common_words)
        return new_string_one, new_string_two
    else:
        return None

