## DOCKERFILE TO BUILD nl2declare_server
# It has to be in the root directory for practical reasons:
# We also have to include the nl2declare library in the container and docker
# does not allow copying it into the container if the Dockerfile is inside
# nl2declare_server (you cannot COPY or ADD anything from parent directories)

# Use an official Python runtime as a parent image
FROM python:3.9.18-slim

# Create directory for storing the gensim and stanza data
WORKDIR /app/data/gensim-data
WORKDIR /app/data/stanza_resources

RUN ln -s /app/data/gensim-data /root/gensim-data
RUN ln -s /app/data/stanza_resources /root/stanza_resources

# Set the working directory to /app
WORKDIR /app

# Copy the directory with the flask server into the container at /app
COPY ./nl2declare_server /app/nl2declare_server
# Copy the library into the container at /app
COPY ./nl2declare /app/nl2declare

# Install any needed packages specified in requirements.txt of the nl2declare library
RUN pip install --no-cache-dir -r nl2declare/requirements.txt

# Install any needed packages specified in requirements.txt of the server
RUN pip install --no-cache-dir -r nl2declare_server/requirements.txt

# Run the library once to already download the models
RUN python -c 'import nl2declare'

# Run app.py when the container launches
CMD ["python", "-m", "nl2declare_server"]