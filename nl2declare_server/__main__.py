import csv
from flask import Flask, request, jsonify
from flask_cors import CORS
from io import StringIO

from nl2declare import conformance_checking, run_on_text
from nl2declare.utils.word_classes import WordClasses

app = Flask(__name__)
CORS(app)

@app.route('/process_json', methods=['POST'])
def process_json():
    try:
        # Get JSON data from the request
        request_data = request.get_json()

        wc = WordClasses()
        mandatory_modals = ", ".join(wc.MANDATORY_MODALS)

        # Process the JSON data (you can customize this part)
        processed_data = {
            'processed_attribute': request_data.get('name', 'Default'),
            'mandatory_modals': mandatory_modals,
        }

        # Return the processed data as JSON response
        return jsonify(processed_data), 200

    except Exception as e:
        # Handle exceptions if any
        error_message = {'error': str(e)}
        return jsonify(error_message), 400

@app.route('/process_single_string', methods=['POST'])
def process_single_string():
    try:
        
        request_json = request.get_json()
        request_string = request_json.get('string')
        request_xes = request_json.get('xes')
        print(f'XES LOG attached to request: {request_xes}')

        if request_string is None:
            raise Exception("request has to contain 'string' field.")
        if request_xes is None:
            raise Exception("request has to contain 'xes' field.")
        
        nodes,edges,declare_export = run_on_text(request_string, request_xes)
        processed_data = {
            'graph_data': {
                'nodes': nodes,
                'edges': edges,
            },
            'declare_export': declare_export
        }
        
        return jsonify(processed_data), 200

    except Exception as e:
        # Handle expections if any
        error_message = {'error': str(e)}
        return jsonify(error_message), 400

@app.route('/run_conformance_checking', methods=['POST'])
def run_conformance_checking():
    try:
        request_json = request.get_json()
        request_string = request_json.get('string')
        request_xes = request_json.get('xes')
        if request_string is None:
            raise Exception("request has to contain 'string' field.")
        if request_xes is None:
            raise Exception("request has to contain 'xes' field.")

        conformance_value = conformance_checking(request_string, request_xes)
        processed_data = {
            'conformance_value': conformance_value
        }
        
        return jsonify(processed_data), 200

    except Exception as e:
        # Handle expections if any
        error_message = {'error': str(e)}
        return jsonify(error_message), 400

@app.route('/preprocess_csv', methods=['POST'])
def preprocess_csv():
    try:
        request_data = request.get_json().get('string')

        if request_data is None:
            raise Exception("csv preprocessing request has to contain 'string' field.")
        
        csv_file = StringIO(request_data)
        # Read the CSV data
        reader = csv.DictReader(csv_file, delimiter=';')

        #if we have a "used constraint" column we use all data from that column, else we return empty
        if 'Used constraint' in reader.fieldnames:
            text_content = ' '.join([row['Used constraint'] for row in reader])
        else:
            text_content = '' # TODO: get content from other column

        print(text_content)
        return jsonify({'preprocessed_csv': text_content}), 200
    except Exception as e:
        # Handle expections if any
        error_message = {'error': str(e)}
        return jsonify(error_message), 400

if __name__ == '__main__':
    # Run the Flask app
    app.run(host='0.0.0.0', debug=True, port=8080)