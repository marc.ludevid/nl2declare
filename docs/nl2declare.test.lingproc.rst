nl2declare.test.lingproc package
================================

Submodules
----------

nl2declare.test.lingproc.test\_declare\_constructor module
----------------------------------------------------------

.. automodule:: nl2declare.test.lingproc.test_declare_constructor
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.test.lingproc.test\_text\_parser module
--------------------------------------------------

.. automodule:: nl2declare.test.lingproc.test_text_parser
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.test.lingproc
   :members:
   :undoc-members:
   :show-inheritance:
