from nl2declare.lingproc.declare_constructor import DeclareConstructor
from nl2declare.constructs.declare_model import DeclareModel
from nl2declare.constructs.text_model import TextModel
from nl2declare.lingproc.text_parser import TextParser

"""
    This class is just for testing.
"""

def test_declare_constructor_succession(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("A claim must be created before an invoice must be sent.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.SUCCESSION, A=Action: create claim, B=Action: send invoice]" in all_constraints 

def test_declare_constructor_response(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("A claim is created after an invoice must be sent.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.RESPONSE, A=Action: create claim, B=Action: send]" in all_constraints 

def test_declare_constructor_precedence(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("Sending an invoice is required to create a claim.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: send invoice, B=Action: create claim]" in all_constraints 


def test_declare_constructor_start(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("The process starts with sending an invoice.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.INIT, A=Action: send invoice]" in all_constraints

def test_declare_constructor_start2(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("Before creating an Email, the process starts.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.INIT, A=Action: create email]" in all_constraints

def test_declare_constructor_end(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("The process ends with sending an invoice.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.END, A=Action: send invoice]" in all_constraints 

def test_declare_constructor_end2(capsys):
    textparser = TextParser()
    constructor = DeclareConstructor()
    text_result = textparser.parse_constraint_string("After creating an Email, the process ends.")
    declare_result = constructor.convert_to_declare_model(text_result)

    all_constraints = ""
    for i in declare_result.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.END, A=Action: create email]" in all_constraints 

