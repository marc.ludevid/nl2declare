class WordClasses:
    """
    This class is used to categorized found words (especially verbs) into different word classes. 
    E.g. differentiate between mandatory and optional modal or "normal" verbs and flow verbs.
    """

    def __init__(self):
        self.MANDATORY_MODALS = ["must", "will", "would", "shall", "should", "require", "have to"]
        self.OPTIONAL_MODALS = ["can", "could", "may", "might"]
        self.START_VERBS = ["start", "begin"]
        self.END_VERBS = ["end", "finish"]
        self.PROCESS_OBJECTS = ["process", "the process", "workflow", "instance", "case"]
        self.FLOW_VERBS = ["require", "need", "precede", "succeed", "follow", "occur", "happen", "take place"]
        self.REVERSE_CLAUSES = ["first", "before", "earlier"]
        self.REVERSE_MARKERS = ["after", "later"]


    def is_mandatory(self,modal):
        return (modal.lower() in [m.lower() for m in self.MANDATORY_MODALS])
    
    def is_optional(self,modal):
        return (modal.lower() in [m.lower() for m in self.OPTIONAL_MODALS])
    
    def is_modal(self,modal):
        return (self.is_optional(modal) or self.is_mandatory(modal))
    
    def is_process_object(self,modal):
        return (modal.lower() in [m.lower() for m in self.PROCESS_OBJECTS])
    
    def is_flow_verb(self, modal):
        return (modal.lower() in [m.lower() for m in self.FLOW_VERBS])
    
    def is_start_verb(self,modal):
        return (modal.lower() in [m.lower() for m in self.START_VERBS])
    
    def is_end_verb(self,modal):
        return (modal.lower() in [m.lower() for m in self.END_VERBS])
    
    def is_reverse_clause(self, modal):
        return (modal.lower() in [m.lower() for m in self.REVERSE_CLAUSES])
    
    def is_reverse_marker(self, modal):
        return (modal.lower() in [m.lower() for m in self.REVERSE_MARKERS])
    
    
    

    
