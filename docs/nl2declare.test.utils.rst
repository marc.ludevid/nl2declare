nl2declare.test.utils package
=============================

Submodules
----------

nl2declare.test.utils.test\_io module
-------------------------------------

.. automodule:: nl2declare.test.utils.test_io
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.test.utils.test\_wordclasses module
----------------------------------------------

.. automodule:: nl2declare.test.utils.test_wordclasses
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.test.utils
   :members:
   :undoc-members:
   :show-inheritance:
