import nl2declare.__main__ as Main
import os

"""
    This class is just for testing.
"""

def test_main_run_single_constraint(capsys):
    result_model = Main.run_single_constraint("A claim must be created before an invoice must be sent.")
    all_constraints = ""
    for i in result_model.constraints:
        all_constraints = all_constraints + i.__str__()
    assert "Constraint [type=ConstraintType.SUCCESSION, A=Action: create claim, B=Action: send invoice]" in all_constraints 

def test_main_run_approach_on_csv_file(capsys):

    result_models = Main.run_approach_on_csv_file("nl2declare/input/datacollectionforpytest.csv")

    all_constraints = ""
    for declare_model in result_models:
        for constraint in declare_model.constraints:
            all_constraints = all_constraints + constraint.__str__()

    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: create claim, B=Action: approve it]" in all_constraints  
    assert "Constraint [type=ConstraintType.RESPONSE, A=Action: select vendor, B=Action: generate purchase order]" in all_constraints 
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: select vendor, B=Action: generate purchase order]" in all_constraints 
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: select vendor, B=Action: send]" in all_constraints 
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: generate purchase order, B=Action: deliver product]" in all_constraints 
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: generate purchase order, B=Action: receive]" in all_constraints 
    assert "Constraint [type=ConstraintType.RESPONSE, A=Action: send invoice, B=Action: create claim]" in all_constraints
    assert "Constraint [type=ConstraintType.PRECEDENCE, A=Action: create claim, B=Action: send invoice]" in all_constraints 




