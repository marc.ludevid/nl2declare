from nl2declare.constructs.text_model import TextModel
from nl2declare.constructs.noun_phrase import NounPhrase
from nl2declare.constructs.interrelation import Interrelation
from nl2declare.constructs.relation_type import RelationType
from nl2declare.constructs.action import Action
from nl2declare.utils.word_classes import WordClasses
import stanza
from stanza.pipeline.core import DownloadMethod
import logging

class TextParser:
    """
    This class consists of various functions used for extracting constraints out of text. 
    """
    
    nlp_pipeline = None

    def __init__(self):
        self.initialize()

    def initialize(self):
        print("Loading NLP tools.")
        #only print something from stanza if a warning or worse happens
        logging.getLogger('urllib3').setLevel(logging.WARNING)
        logging.getLogger('stanza').setLevel(logging.WARNING)

        if TextParser.nlp_pipeline is None:
            TextParser.nlp_pipeline = stanza.Pipeline(lang='en', processors='tokenize,pos,lemma,depparse', download_method=DownloadMethod.REUSE_RESOURCES)
        self.nlp = TextParser.nlp_pipeline

        print("Finished loading NLP tools.")

    def parse_constraint_string(self, str):
        model = TextModel(str)
        self.extract_noun_phrases(model)
        self.extract_verbs(model)
        self.analyze_grammar(model)
        self.analyze_flow_actions(model)
        self.post_relation_processing(model)
        return model

    def build_noun_phrase(self, noun, sentence):
        current_noun_phrase = []
        current_np_start_index = noun.id
        current_np_end_index = noun.id
        for dep in sentence.dependencies:
            if(dep[0].id == noun.id and dep[1]=='compound'):
                current_noun_phrase.append(dep[2].text)
                if current_np_start_index == noun.id: #if we did not add an artice or adjective before
                    current_np_start_index = dep[2].id
                
        current_noun_phrase.append(noun.text)
        return (" ".join(current_noun_phrase), current_np_start_index, current_np_end_index)

    def process_complex_noun_phrases(self, noun_phrases, complex_nouns, sentence):
        for (noun1,noun2) in complex_nouns: #means we have (noun1, nmod, noun2) dependency
            (noun_phrase1, np1_start, np1_end) = self.build_noun_phrase(noun1, sentence)
            (noun_phrase2, np2_start, np2_end) = self.build_noun_phrase(noun2, sentence)
            for dep in sentence.dependencies:
                if(dep[0].id == noun2.id and dep[1]=='case'):
                    noun_phrases.append((noun_phrase1 + " " + noun_phrase2, np1_start, np2_end))
        return noun_phrases
    
    def extract_noun_phrases(self, model):
        """
        extract_noun_phrases: Extracts noun phrases out of the text of the given model and adds those as noun phrases to the model. 

        Parameters:
            - model (TextModel): A textmodel.
        """
        doc = self.nlp(model.text)

        #auxiliary lists
        prop_nouns = [] 
        nouns = [] 
        noun_phrases = []  #store triplets here: (noun_phrase, start_index, end_index)
        complex_nouns = [] #contains tuples of nouns which have an nmod relation
        covered_nouns = [] #need this to intermediately store there nouns which are part of a complex noun phrases

        for sentence in doc.sentences:
            #find all nouns, pronouns, proper nouns in the sentence and store them in respective list
            for word in sentence.words:
                if 'NOUN' in word.upos:
                    nouns.append(word)
                if 'PROPN' in word.upos:
                    prop_nouns.append(word)
                if 'PRON' in word.upos:
                    if word.text not in noun_phrases:
                        noun_phrases.append((word.text, word.id, word.id)) 

            #for each found noun, search the sentences' dependencies for occurrences of this noun to build the noun_phrases
            for noun in nouns:
                if(noun not in covered_nouns):
                    current_noun_phrase = []
                    current_np_start_index = noun.id
                    current_np_end_index = noun.id
                    already_covered = False
                    for dep in sentence.dependencies:
                        if(dep[0].id == noun.id and dep[1]=='compound'): #e.g. case id 
                            current_noun_phrase.append(dep[2].text)
                            if current_np_start_index == noun.id: #if we did not add an artice or adjective before
                                current_np_start_index = dep[2].id
                        if(dep[2].id == noun.id and dep[1]=='compound'): #do not process nouns which are part of a compunded noun and therefore have already been covered
                            already_covered = True
                        if(dep[0].id == noun.id and dep[1]=='nmod'): #for complex noun phrases (creation of a claim) -> shift the involved nouns in separate list and process later
                            covered_nouns.append(dep[0]) #cover these nouns separately in process_complex_nouns
                            covered_nouns.append(dep[2])
                            complex_nouns.append((dep[0],dep[2])) #save information which two nouns are nmod-related
                            already_covered = True
                    if(not already_covered):
                        current_noun_phrase.append(noun.text)
                        noun_phrases.append((" ".join(current_noun_phrase), current_np_start_index, current_np_end_index))
            
            #process the complex noun phrases like "creation of a document"
            if(not complex_nouns == []):
                noun_phrases = self.process_complex_noun_phrases(noun_phrases, complex_nouns, sentence)

            #for each found proper noun (like names), check if they are compounded and add them to the list of noun_phrases
            for prop_noun in prop_nouns:
                current_noun_phrase = []
                current_np_start_index = prop_noun.id
                current_np_end_index = prop_noun.id
                already_covered = False
                for dep in sentence.dependencies:
                    if(dep[0].id == prop_noun.id and dep[1]=='flat'): # for names like Pia Marie Halkour
                        current_noun_phrase.append(dep[2].text)
                        current_np_end_index = dep[2].id
                    if(dep[2].id == prop_noun.id and dep[1]=='flat'):
                        already_covered = True
                    if(dep[0].id == prop_noun.id and dep[1]=='compound'): # for names like Albert Einstein
                        current_noun_phrase.append(dep[2].text)
                        current_noun_phrase.append(dep[0].text)
                        current_np_start_index = dep[2].id
                        current_np_end_index = dep[0].id
                        noun_phrases.append((" ".join(current_noun_phrase), current_np_start_index, current_np_end_index))
                        already_covered = True
                    if(dep[2].id == prop_noun.id and dep[1]=='compound'):
                        already_covered = True
                if(not already_covered):        
                    current_noun_phrase.insert(0, prop_noun.text)
                    noun_phrases.append((" ".join(current_noun_phrase), current_np_start_index, current_np_end_index))

        for (noun_phrase, start_index, end_index) in noun_phrases:  
            model.noun_phrases.append(NounPhrase(noun_phrase, start_index, end_index))
        

    #java_implementation: extractActivities()
    def extract_verbs(self, model):
        """
        extract_verbs: Extracts verbs out of the text of the given model and adds those as actions to the model. 

        Parameters:
            - model (TextModel): A textmodel.
        """
        doc = self.nlp(model.text)
        lemmas = [] #list for basic verbs
        poss = []   #list for word categorization
        passive_flags=[]
        wordclass = WordClasses()

        #iterate through every word of every sentences
        for sentence in doc.sentences:
            for word in sentence.words:
                lemmas.append(word.lemma) 
                poss.append(word.upos) #save categorization of every word like noun, verb ...
                if "Voice=Pass" in str(word.feats):
                    passive_flags.append(1)
                else:
                    passive_flags.append(0)
            for i in range(len(lemmas)): #for every lemma of a word
                pos = poss[i]
                passive = passive_flags[i]
                lemma = lemmas[i]
                if pos == 'VERB': #if word is categorized as a verb 
                    if lemma != 'have' and lemma != 'be':
                        found_action = Action(lemma, i+1)
                        if wordclass.is_flow_verb(lemma):
                            found_action.is_flow_action = True
                        if passive == 1:
                            found_action.is_passive = True
                        model.add_action(found_action)



    #java_implementation: stanfordParsing()
    def analyze_grammar(self, model):
        """
        analyze_grammar: Enriches the inputted text model with several additonal information, e.g. modal verbs, markers, conjunctions, negations, object, subject. Already builds interrelations for adverbial clauses.

        Parameters:
            - model (TextModel): A textmodel.
        """
         
        doc = self.nlp(model.text)

        for sentence in doc.sentences:
            # Additional processing for typed dependencies: in case dep[0] is an action (verb)
            for dep in sentence.dependencies:
                reln = dep[1]
                gov = dep[0]
                govA = model.get_action_by_verb_id(gov.id) #none if gov is not a verb
                dep_word = dep[2]

                if reln == 'nsubj:pass' or reln == 'obj':
                    object = model.get_noun_phrase_by_index(dep_word.id)
                    if(govA != None):
                        govA.object = object
                elif reln == 'nsubj':
                    if(govA != None):
                        govA.subject = model.get_noun_phrase_by_index(dep_word.id)
                elif reln == 'csubj': #subject of action is also an action (gerund), e.g. approving a claim occors... 
                    if(govA != None):
                        govA.subject = model.get_action_by_verb_id(dep_word.id)
                elif reln == 'csubj:pass':
                    if(govA != None):
                        govA.object = model.get_action_by_verb_id(dep_word.id)
                elif reln == 'aux':
                    if(govA != None):
                        govA.modal = dep_word.text
                elif reln == 'neg':
                    if(govA != None):
                        govA.is_negative = True
                elif reln == 'mark':
                    if(govA != None):
                        govA.marker = dep_word.text
                elif reln == 'advmod':
                    if(dep_word.text == 'not'):
                        if(govA != None):
                            govA.is_negative = True
                    elif(govA != None):
                        govA.clause = dep_word.text
                elif reln == 'conj':
                    actB = model.get_action_by_verb_id(dep_word.id)
                    if(govA != None and actB != None):
                        govA.conjunctions.add(actB)
                        actB.conjunctions.add(govA)

        # Additional processing for adverbial clauses (between two (non-flow) actions)
        for dep in sentence.dependencies:
            reln = dep[1]
            gov = dep[0]
            dep_word = dep[2]
            govA = model.get_action_by_verb_id(gov.id) #none if gov is not a verb
            depA = model.get_action_by_verb_id(dep_word.id) #none if dep_word is not a verb
            actionA = None
            actionB = None
            aActions = set()
            bActions = set()

            if reln == 'advcl' and govA != None and depA != None: 
                if(govA.verb_id < depA.verb_id):
                    actionA = govA
                    actionB = depA
                else:
                    actionA = depA
                    actionB = govA
                
                aActions.add(actionA)
                aActions.update(actionA.conjunctions)
                bActions.add(actionB)
                bActions.update(actionB.conjunctions)

                if(actionA != None and not actionA.is_flow_action):
                    for a in aActions:
                        for b in bActions:
                            interr = Interrelation(a, b, RelationType.FOLLOWS)
                            if(a.object == None):
                                neighbor = self.find_neighbor_action(model, a, bActions)
                                self.merge_incomplete_actions(a, neighbor)
                            
                            if(interr not in model.interrelations):
                                model.interrelations.append(interr)


    def find_neighbor_action(self, model, action, bActions):
        """
        find_neighbor_action: Finds the closest neighbor action to the given action within a model, excluding actions in the specified set.

        Parameters:
        - model (TextModel): The model containing a collection of actions.
        - action (Action): The target action for which the closest neighbor needs to be found.
        - bActions ({Action}): A set of actions to be excluded from consideration.

        Returns:
        The closest neighbor action to the given action, or None if no eligible neighbor is found.
        """
        closest = None
        mindist = float('inf')

        for candidate in model.actions:
            if candidate != action and candidate not in bActions:
                dist = abs(candidate.verb_id - action.verb_id)

                if dist < mindist:
                    closest = candidate
                    mindist = dist

        return closest

    def merge_incomplete_actions(self, nullaction, neighbor):
        """
        Merges information from a neighboring action into an incomplete (null) action.

        Parameters:
        - nullaction (Action): The incomplete action to be updated.
        - neighbor(Action): The neighboring action providing additional information.

        The method updates the 'object' attribute of the nullaction with the 'object' attribute of the neighbor.
        If the verb of the nullaction is "be", "have", or "have to", it also updates the 'verb' and 'verb_id' attributes 
        with those of the neighbor. Additionally, if the nullaction has a modal, it updates the 'modal' attribute to 
        match the nullaction's original 'verb', and updates the 'verb' and 'verb_id' attributes with those of the neighbor.
        """
        if neighbor is not None:
            nullverb = nullaction.verb
            nullaction.object = neighbor.object 
            if(nullverb == "be" or nullverb == "have" or nullverb == "have to"):
                nullaction.verb = neighbor.verb
                nullaction.verb_id = neighbor.verb_id
            if(nullaction.modal != None):
                nullaction.modal = nullverb
                nullaction.verb = neighbor.verb
                nullaction.verb_id = neighbor.verb_id

    # java_implementation: nounbasedProcessing
    def analyze_flow_actions(self, model):
        """
        Analyzes and processes flow actions within the given model, adjusting actions and interrelations accordingly.

        Parameters:
        - model (TextModel): The model containing actions and noun phrases.

        The method identifies flow actions within the model and modifies them based on specific patterns. It also updates
        interrelations between actions. The resulting model may have new actions, removed actions, and updated interrelations.
        """
        wordclasses = WordClasses()
        new_actions = []
        removed_actions = []
        subj_action = None
        obj_action = None
        action2 = None

        no_of_flow_actions = 0
        for action in model.actions:
            if action.is_flow_action:
                no_of_flow_actions += 1

        if(no_of_flow_actions == 1):
            for action in model.actions:
                modal = None
                if(action.is_flow_action):    
                    if(action.modal is not None):
                        modal = action.modal
                    if(action.subject is not None): #retrieve subject (if exists) of flowverb and store it as action object
                        if(isinstance(action.subject, Action)):
                            subj_action = Action(action.subject.action_str(), -1) #verb_id = -1 signals that this action is a real action (verb)
                        elif(isinstance(action.subject, NounPhrase)):
                            subj_action = Action(str(action.subject), -2) #verb_id = -2 signals that this action a NounPhrase                            
                    if(action.object is not None): #retrieve object
                        if(isinstance(action.object, Action)):
                            obj_action = Action(action.object.action_str(), -1)                            
                        elif(isinstance(action.object, NounPhrase)):
                            obj_action = Action(str(action.object), -2)                      

                    if(action.verb.startswith("occur") or action.verb.startswith("happen") or action.verb.startswith("take place") or action.verb.startswith("take place")): #in this case: only subject exists
                        if(subj_action is not None and obj_action is None):
                            doc = self.nlp(model.text)
                            for sentence in doc.sentences:
                                for dep in sentence.dependencies:
                                    if(dep[0].id == action.verb_id and dep[1] == 'obl' or dep[1] == 'advcl'):
                                        for dep2 in sentence.dependencies:
                                            if(dep[2].id == dep2[0].id and ((dep[1] == 'obl' and dep2[1] == 'case') or (dep[1] == 'advcl' and dep2[1] == 'mark'))):
                                                if(isinstance(action.subject, Action)):
                                                    removed_actions.append(action.subject)
                                                    subj_action = Action(action.subject.action_str(), -3) #-3 as code for postprocessing
                                                    subj_action.modal = modal
                                                else:
                                                    subj_action = Action(str(action.subject), -3) #-3 as code for postprocessing
                                                    subj_action.modal = modal
                                                if(wordclasses.is_reverse_clause(dep2[2].text)): #first, before, earlier
                                                    subj_action.clause = "first"
                                                elif(wordclasses.is_reverse_marker(dep2[2].text)):
                                                    subj_action.clause = "after"
                                                new_actions.append(subj_action)
                                                if(dep[2].upos == "NOUN"): #for postprocessing
                                                    action2 = model.get_noun_phrase_by_index(dep[2].id)
                        else:
                            print("ERROR: CASE NOT IMPLEMENTED YET")


                    elif(action.verb.startswith("succeed") or action.verb.startswith("require") or action.verb.startswith("need") or action.verb.startswith("follow") or action.verb.startswith("preced")):
                        if(action.is_passive): #in this case: only an object exists
                            if(subj_action is None and obj_action is not None):
                                if(isinstance(action.object, Action)):
                                    removed_actions.append(action.object)
                                    obj_action = Action(action.object.action_str(), -4) #-4 as code for postprocessing
                                    obj_action.modal = modal
                                else:
                                    obj_action = Action(str(action.object), -4) #-4 as code for postprocessing
                                    obj_action.modal = modal
                                if(action.verb.startswith("succeed") or action.verb.startswith("require") or action.verb.startswith("need") or action.verb.startswith("follow")):
                                    obj_action.clause = "first"
                                    if(action.verb.startswith("require") or action.verb.startswith("need")): #action/np is required/needed before/after action/np
                                        doc = self.nlp(model.text)
                                        for sentence in doc.sentences:
                                            for dep in sentence.dependencies:
                                                if(dep[0].id == action.verb_id and dep[1] == 'obl' or dep[1] == 'advcl'):
                                                    for dep2 in sentence.dependencies:
                                                        if(dep[2].id == dep2[0].id and ((dep[1] == 'obl' and dep2[1] == 'case') or (dep[1] == 'advcl' and dep2[1] == 'mark'))):
                                                            if(wordclasses.is_reverse_clause(dep2[2].text) or wordclasses.is_reverse_marker(dep2[2].text)): #ensure that a marker/clause is in the sentence
                                                                if(isinstance(action.subject, Action)):
                                                                    removed_actions.append(action.object)
                                                                    obj_action = Action(action.object.action_str(), -3) #-3 as code for postprocessing
                                                                    obj_action.modal = modal
                                                                else:
                                                                    obj_action = Action(str(action.object), -3) #-3 as code for postprocessing
                                                                    obj_action.modal = modal
                                                                if(wordclasses.is_reverse_clause(dep2[2].text)): #first, before, earlier
                                                                    obj_action.clause = "first"
                                                                elif(wordclasses.is_reverse_marker(dep2[2].text)):
                                                                    obj_action.clause = "after"
                                                                    obj_action.modal = "must"
                                                                if(dep[2].upos == "NOUN"): #for postprocessing
                                                                    action2 = model.get_noun_phrase_by_index(dep[2].id)
                                elif(action.verb.startswith("preced")):
                                    obj_action.clause = "after"
                                    if(wordclasses.is_mandatory(modal)): #if we have passive and mandatory modal, the object action is actually not mandatory (e.g. creation of claim must be preceded by sending an invoice)
                                        obj_action.modal ="can"
                                new_actions.append(obj_action)
                            else:
                                print("ERROR: CASE NOT IMPLEMENTED YET")
                        else: #not passive: object and subject exist => build interrelations directly 
                            if(subj_action is not None and obj_action is not None):
                                subj_action.modal = modal
                                if(action.verb.startswith("preced")):
                                        model.interrelations.append(Interrelation(subj_action, obj_action, RelationType.FOLLOWS))
                                elif(action.verb.startswith("follow") or action.verb.startswith("succeed") or action.verb.startswith("require") or action.verb.startswith("need")):
                                        model.interrelations.append(Interrelation(obj_action, subj_action, RelationType.FOLLOWS))
                            elif(subj_action is not None and obj_action is None): #object of the flowverb is not classified as such since it is a verb -> find it manually
                                subj_action.modal = modal
                                doc = self.nlp(model.text)
                                for sentence in doc.sentences:
                                    for dep in sentence.dependencies:
                                        if(dep[0].id == action.verb_id): #find current flow verb
                                            if dep[1] == 'advcl' or dep[1]== 'xcomp':
                                                obj_action = model.get_action_by_verb_id(dep[2].id)
                                                if(action.verb.startswith("preced")):
                                                        model.interrelations.append(Interrelation(subj_action, obj_action, RelationType.FOLLOWS))
                                                elif(action.verb.startswith("follow") or action.verb.startswith("succeed") or action.verb.startswith("require") or action.verb.startswith("need")):
                                                        model.interrelations.append(Interrelation(obj_action, subj_action, RelationType.FOLLOWS))
                                
                    
                    #update interrelations
                    if(subj_action is not None):
                        for interr in model.interrelations:
                            if(interr.action_a == action):
                                interr.action_a = subj_action
                            if(interr.action_b == action):
                                interr.action_b = subj_action
                    if(obj_action is not None):
                        for interr in model.interrelations:
                            if(interr.action_a == action):
                                interr.action_a = obj_action
                            if(interr.action_b == action):
                                interr.action_b = obj_action

                    removed_actions.append(action)


        for new_action in new_actions:
            model.actions.append(new_action) 
        
        for remA in removed_actions: #remove all flow actions and subj_actions/obj_actions which were a real action before
            if(remA in model.actions):
                model.actions.remove(remA)

        if(len(model.actions) == 1):
           verb_action = model.actions[0]
           if(verb_action.verb_id == -3): #for case where we have occur, happen,... with only a subject
               if(verb_action.clause is not None):
                   model.actions.remove(verb_action)
                   if(action2 is not None):
                        model.actions.append(Action(action2.text, action2.start_index))
                   model.actions.append(verb_action)
           elif(verb_action.verb_id == -4): 
                model.actions.remove(verb_action)
                for np in model.noun_phrases:
                    if(np.text not in verb_action.verb):
                        model.actions.append(Action(np.text, np.start_index))
                model.actions.append(verb_action)
           else: #default case from java_implementation
                model.actions.remove(verb_action)
                for np in model.noun_phrases:
                    if(verb_action is None or np.start_index < verb_action.verb_id):
                        model.actions.append(Action(np.text, np.start_index))
                    else:
                        model.actions.append(verb_action)
                        verb_action = None

        if not model.actions:
            for np in model.noun_phrases:
                model.actions.append(Action(np.text, np.start_index))


    def reversedCondition(self, actA, actB):
        """
        reversedCondition: Checks if actB is a reverse clause. If yes it returns True, else False.

        Parameters:
            - actA (Action): an action
            - actB (Action): an action

        Returns:
            - boolean value
        """
        #check for reverse order indicators, e.g. "must be created first"
        wordclass = WordClasses()
        if actB.clause is not None and wordclass.is_reverse_clause(actB.clause):
            return True
        if actB.marker is not None and wordclass.is_reverse_marker(actB.marker):
            return True

        return False


    def post_relation_processing(self, model):
        """
        Performs post-processing on interrelations within the given model.

        Parameters:
        - model(TextModel): The model containing actions and interrelations.

        The method checks if there are no interrelations in the model. If there are multiple actions and no interrelations,
        it adds a default 'FOLLOWS' interrelation between the first and last actions. Additionally, it checks each existing
        interrelation to determine if it should be inverted based on specific conditions defined in 'reversedCondition'.
        """
        if not model.interrelations:
            if len(model.actions)>1:
                actA = model.actions[0] #first element 
                actB = model.actions[len(model.actions)-1] #last element
                new_rel = Interrelation(actA,actB,RelationType.FOLLOWS) 
                model.add_interrelation(new_rel)

        for rel in model.interrelations: 
            #check for all interrelations if they should be inverted (detail in reversedCondition())
            if(self.reversedCondition(rel.action_a, rel.action_b)):
                #if yes, revert relation
                currA = rel.action_a
                currB = rel.action_b
                rel.action_a = currB
                rel.action_b = currA
        



