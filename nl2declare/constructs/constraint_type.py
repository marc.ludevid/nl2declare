from enum import Enum, auto

class ConstraintType(Enum):

    """
    This class represents the types of constraints. 
    """

    PRECEDENCE = auto()
    ALTPRECEDENCE = auto()
    RESPONSE = auto()
    NOTRESPONSE = auto()
    SUCCESSION = auto()
    NOTSUCCESSION = auto()
    INIT = auto()
    END = auto()
    ATMOSTONCE = auto()
    EXISTENCE = auto()

    @staticmethod
    def get_type(s):
        for constraint_type in ConstraintType:
            if constraint_type.name.lower() == s.lower():
                return constraint_type
        return None