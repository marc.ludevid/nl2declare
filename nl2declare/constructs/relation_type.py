from enum import Enum

class RelationType(Enum):
    """
    This class represents the possible types of interrelations. 
    """
    
    FOLLOWS = "FOLLOWS"
    XOR = "XOR"
    AND = "AND"