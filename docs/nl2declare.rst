nl2declare package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   nl2declare.actmatching
   nl2declare.application
   nl2declare.constructs
   nl2declare.evaluation
   nl2declare.lingproc
   nl2declare.test
   nl2declare.utils

Module contents
---------------

.. automodule:: nl2declare
   :members:
   :undoc-members:
   :show-inheritance:
