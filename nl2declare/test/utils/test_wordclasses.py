
from nl2declare.utils.word_classes import WordClasses

"""
    This class is just for testing.
"""

def test_wordclasses_is_optional(capsys):
    wc = WordClasses()
    result = wc.is_optional("can")
    result2 = wc.is_optional("would")
    assert (result==True) and (result2==False)

def test_wordclasses_is_modal(capsys):
    wc = WordClasses()
    result = wc.is_modal("will")
    result2 = wc.is_modal("need")
    assert (result==True) and (result2==False)

def test_wordclasses_is_process_object(capsys):
    wc = WordClasses()
    result = wc.is_process_object("workflow")
    result2 = wc.is_process_object("finish")
    assert (result==True) and (result2==False)

def test_wordclasses_is_flow_verb(capsys):
    wc = WordClasses()
    result = wc.is_flow_verb("need")
    result2 = wc.is_flow_verb("send")
    assert (result==True) and (result2==False)

def test_wordclasses_is_start_verb(capsys):
    wc = WordClasses()
    result = wc.is_start_verb("start")
    result2 = wc.is_start_verb("end")
    assert (result==True) and (result2==False)

def test_wordclasses_is_end_verb(capsys):
    wc = WordClasses()
    result = wc.is_end_verb("end")
    result2 = wc.is_end_verb("start")
    assert (result==True) and (result2==False)

