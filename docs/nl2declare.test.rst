nl2declare.test package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   nl2declare.test.lingproc
   nl2declare.test.utils

Submodules
----------

nl2declare.test.test\_\_main\_\_ module
---------------------------------------

.. automodule:: nl2declare.test.test__main__
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.test
   :members:
   :undoc-members:
   :show-inheritance:
