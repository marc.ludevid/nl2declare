from __future__ import annotations
import numpy as np
from transformers import BertModel, BertTokenizer
import torch
from scipy.spatial.distance import cosine

"""
    This class uses bert to calculate the similarity between word phrases.
"""

def get_bert_embeddings(sentence, model, tokenizer):
    tokens = tokenizer.encode(sentence, add_special_tokens=True)
    tokens_tensor = torch.tensor([tokens])

    with torch.no_grad():
        outputs = model(tokens_tensor)

    embeddings = outputs.last_hidden_state.squeeze(0)

    return embeddings

def get_cosine_similarity(embeddings1, embeddings2):
    return 1 - cosine(embeddings1.mean(dim=0), embeddings2.mean(dim=0))

def get_most_similar_activity(activity: str, activities: list[str]):
    # Use BERT to extract the most similar activity from the list of activites
    # to match them
    model = BertModel.from_pretrained('bert-large-uncased')
    tokenizer = BertTokenizer.from_pretrained('bert-large-uncased')

    embedding_activity = get_bert_embeddings(activity, model, tokenizer)
    
    similarities = []
    for act in activities:
        #get embedding of act
        second_embedding_activity = get_bert_embeddings(act,model,tokenizer)
        #calculate similarity of the two embeddings
        similarity_value =get_cosine_similarity(embedding_activity, second_embedding_activity)
        similarities.append(similarity_value)

    return activities[np.argmax(similarities)], max(similarities)


def get_most_similar_activity_without_common_words(activity: str, activities: list[str]):
    # Use BERT to extract the most similar activity from the list of activites
    # to match them
    model = BertModel.from_pretrained('bert-base-uncased')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

    similarities = []

    for act in activities:
        if have_common_word(activity,act):
            #remove common words in the strings
            new_start_word, new_second_word = remove_common_words(activity,act)
            #get embeddings of adapted strings
            start_embedding_activity = get_bert_embeddings(new_start_word,model,tokenizer)
            second_embedding_activity = get_bert_embeddings(new_second_word,model,tokenizer)
            #calculate similarity of the two embeddings
            similarity_value =get_cosine_similarity(start_embedding_activity, second_embedding_activity)
            similarities.append(similarity_value)
        else:
            similarities.append(0)

    return activities[np.argmax(similarities)], max(similarities)


def have_common_word(stringOne, stringTwo):
    wordsOne = set(stringOne.split())
    wordsTwo = set(stringTwo.split())
    common_words = wordsOne.intersection(wordsTwo)

    # Return True if there is at least one common word, else False
    return bool(common_words)


def remove_common_words(stringOne, stringTwo):
    wordsOne = set(stringOne.split())
    wordsTwo = set(stringTwo.split())
    common_words = wordsOne.intersection(wordsTwo)

    # If there are common words, remove them and return the strings
    if common_words:
        new_string_one = ' '.join(word for word in wordsOne if word not in common_words)
        new_string_two = ' '.join(word for word in wordsTwo if word not in common_words)
        return new_string_one, new_string_two
    else:
        return None


