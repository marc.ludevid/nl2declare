nl2declare.constructs package
=============================

Submodules
----------

nl2declare.constructs.action module
-----------------------------------

.. automodule:: nl2declare.constructs.action
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.constraint\_type module
---------------------------------------------

.. automodule:: nl2declare.constructs.constraint_type
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.declare\_constraint module
------------------------------------------------

.. automodule:: nl2declare.constructs.declare_constraint
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.declare\_model module
-------------------------------------------

.. automodule:: nl2declare.constructs.declare_model
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.interrelation module
------------------------------------------

.. automodule:: nl2declare.constructs.interrelation
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.noun\_phrase module
-----------------------------------------

.. automodule:: nl2declare.constructs.noun_phrase
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.relation\_type module
-------------------------------------------

.. automodule:: nl2declare.constructs.relation_type
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.constructs.text\_model module
----------------------------------------

.. automodule:: nl2declare.constructs.text_model
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.constructs
   :members:
   :undoc-members:
   :show-inheritance:
