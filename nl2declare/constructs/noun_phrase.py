class NounPhrase:
    """
    This class represents noun phrases. They have a start and end index based on their position in a sentence.  
    """
    
    def __init__(self, text="", start_index=-1, end_index=-1):
        self.text = text
        self.start_index = start_index
        self.end_index = end_index

    def in_span(self, index):
        return self.start_index <= index <= self.end_index

    def __str__(self):
        return self.text

    def is_empty(self):
        return self.text == ""