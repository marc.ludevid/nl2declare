import re
import csv
import xml.etree.ElementTree as ET
from nl2declare.lingproc.text_parser import TextParser
from nl2declare.lingproc.declare_constructor import DeclareConstructor
from nl2declare.actmatching.activity_matching_word2vec import *
from nl2declare.constructs.declare_model import DeclareModel
from nl2declare.constructs.constraint_type import ConstraintType
from nl2declare.constructs.action import Action
from nl2declare.application.d4py import *
from io import StringIO
import tempfile
import pathlib

parser = TextParser()

def run_on_csv(csv_as_string, target_activity_event_log):
    """
    run_on_csv: Extracts declare constraints out of a CSV file.

    This function takes a CSV-formatted string (`csv_as_string`) out of the column "Used Constraint" and an
    event log. If the column does not exist, empty lists are returned. Else  (nodes,edges). If the event log has a column "Concept:Name" , the activites 
    from the constraints are matched with those labels. As third element also a .decl file of the constraints is returned.

    Parameters:
        - csv_as_string (str): A CSV-formatted string containing data to be processed.
        - target_activity_event_log (str): The target_activity_event_log log for processing or an empty string.

    Returns:
        - Nodes: All actions from the found constraints as nodes for a declare graph
        - Edges: All relations from the found constraints as edges for a declare graph
        - .decl file: The found declare constraints as d4py file (.decl). This file can be used for further examinations using d4py.

    Notes:
        - If the "Used constraint" column is not present, an empty tuple is returned.
        - The target_activity_event_log can be empty
        - If the target_activity_event_log is not empty, all labels that should be matched need to be i a column called "Concept:Name"
    """
    #Create a file-like object from the string
    csv_file = StringIO(csv_as_string)
    # Read the CSV data
    reader = csv.DictReader(csv_file)

    #if we have a "used constraint" column we use all data from that column, else we return empty
    if 'Used constraint' in reader.fieldnames:
        text_content = ''.join([row['Used constraint'] for row in reader])
    else:
        return ([],[],[]) #empty if no column called "Used constraint"

    return run_on_text(text_content, target_activity_event_log)



def run_on_text(textstring, target_activity_event_log):
    """
    run_on_text: Extracts declare constraints out of a text.

    This function takes a text string (`textstring`) and an event log. If the event log has a column "Concept:Name" , the activites 
    from the constraints are matched with those labels. The found declare constraints from the text are returned in a graph format. 
    As third element also a .decl file of the constraints is returned.

    Parameters:
        - textstring (str): A text string containing sentences to be processed.
        - target_activity_event_log (str): The target_activity_event_log log for processing or an empty string.

    Returns:
        - Nodes: All actions from the found constraints as nodes for a declare graph
        - Edges: All relations from the found constraints as edges for a declare graph
        - .decl file: The found declare constraints as d4py file (.decl). This file can be used for further examinations using d4py.

    Notes:
        - The target_activity_event_log can be empty
        - If the target_activity_event_log is not empty, all labels that should be matched need to be in a column called "Concept:Name"
    """
    all_constraints_set = create_constraint_set_from_text(textstring, target_activity_event_log)

    # Iterate all unary constraints and collect their nodes
    unary_nodes = []
    init_nodes = []
    end_nodes = []
    for con in all_constraints_set:
        if con.type == ConstraintType.INIT:
            action_str = con.action_a.action_str()
            if action_str not in unary_nodes:
                unary_nodes.append(action_str)
            if action_str not in init_nodes:
                init_nodes.append(action_str)
        if con.type == ConstraintType.END:
            action_str = con.action_a.action_str()
            if action_str not in unary_nodes:
                unary_nodes.append(action_str)
            if action_str not in end_nodes:
                end_nodes.append(action_str)
    
    # Create lookup dictionary for the nodes with unary constraints with their new action string
    unary_nodes_name_lookup = {}
    for node in unary_nodes:
        is_init = node in init_nodes
        is_end = node in end_nodes
        if is_init and is_end:
            unary_nodes_name_lookup[node] = "Start and End:\n" + node
        elif is_init and not is_end:
            unary_nodes_name_lookup[node] = "Start:\n" + node
        elif not is_init and is_end:
            unary_nodes_name_lookup[node] = "End:\n" + node
        else:
            raise Exception("Something went wrong during unary constraint extraction")

    # Iterate all constraints and collect the nodes and edges of the graph:
    nodes = []
    edges = []
    for con in all_constraints_set:
        if con.type == ConstraintType.INIT or con.type == ConstraintType.END:
            node = {"name": unary_nodes_name_lookup[con.action_a.action_str()]}
            if node not in nodes:
                nodes.append(node)
        else:
            action_a_str = con.action_a.action_str()
            action_a_str = unary_nodes_name_lookup.get(action_a_str, action_a_str) # If the action has an unary constraint update the action string
            action_b_str = con.action_b.action_str()
            action_b_str = unary_nodes_name_lookup.get(action_b_str, action_b_str) # If the action has an unary constraint update the action string
            node_a = {"name": action_a_str}
            node_b = {"name": action_b_str}
            if node_a not in nodes:
                nodes.append(node_a)
            if node_b not in nodes:
                nodes.append(node_b)
            constraint_str = "!" + con.type.name if con.is_negative else con.type.name
            edge = {"source": action_a_str, "target": action_b_str, "label": {"show": True, "formatter": constraint_str}}
            edges.append(edge)

    declare_export = create_d4py_declaremodel(all_constraints_set)

    return (nodes,edges,declare_export)

def create_constraint_set_from_text(textstring, event_log):
    """
    Create_constraint_set_from_text: Extracts declare constraints out of a text.

    This function takes a text string (`textstring`) and an event log. If the event log has a column "Concept:Name" , the activites 
    from the constraints are matched with those labels.The found declare constraints are returned in a set.

    Parameters:
        - textstring (str): A text string containing sentences to be processed.
        - event_log (str): The target_activity_event_log log for processing or an empty string.

    Returns:
        - Set of found declare_constraints

    Notes:
        - The target_activity_event_log can be empty
        - If the target_activity_event_log is not empty, all labels that should be matched need to be i a column called "Concept:Name"
    """
    declare_constructor = DeclareConstructor()
    all_constraints = []
    all_actions = set()
    MATCHING_THRESHOLD = 0.5

    # Split the text into sentences and remove empty strings 
    sentences = re.split(r'[.!?]', textstring)
    sentences = [sentence.strip() for sentence in sentences if sentence.strip()]

    # Extract constraints from all sentences
    for sentence in sentences:
        text_model_for_sentence = parser.parse_constraint_string(sentence)
        for act in text_model_for_sentence.actions:
            all_actions.add(act.action_str())
        declare_model_for_sentence = declare_constructor.convert_to_declare_model(text_model_for_sentence)
        declare_model_for_sentence.sentence_model = text_model_for_sentence
        # Collect the extracted constraints in all_constraints
        all_constraints.extend(declare_model_for_sentence.constraints)

    all_actions = list(all_actions)

    #perform activity matching
        
    if event_log != "": #if we get an xes file
        target_activity_list = get_activities_from_event_log(event_log)
    else:#if we do not get an xes file
        target_activity_list = []
    
    #first pairwise activity matching
    for const in all_constraints:
            new_act_a = None
            new_act_b = None
            if const.action_a is not None:
                if const.action_b is None:
                    to_check_list = [x for x in all_actions if x != const.action_a.action_str()]
                else:
                    to_check_list = [x for x in all_actions if x != const.action_a.action_str() and x!= const.action_b.action_str()]
                if(to_check_list != []):
                    most_similar_action, similarity_value = get_most_similar_activity_without_common_words(const.action_a.action_str(), to_check_list)
                    if similarity_value >= MATCHING_THRESHOLD:
                        new_act_a = Action(most_similar_action, 0)
                        if const.action_a.action_str() in all_actions:
                            all_actions.remove(const.action_a.action_str())
            if const.action_b is not None:
                if const.action_a is None:
                    to_check_list = [x for x in all_actions if x != const.action_b.action_str()]
                else:
                    to_check_list = [x for x in all_actions if x != const.action_b.action_str() and x!= const.action_a.action_str()]
                if(to_check_list != []):
                    most_similar_action, similarity_value = get_most_similar_activity_without_common_words(const.action_b.action_str(), to_check_list)
                    if similarity_value >= MATCHING_THRESHOLD:
                        new_act_b = Action(most_similar_action, 0)
                        if const.action_b.action_str() in all_actions:
                            all_actions.remove(const.action_b.action_str())
            if new_act_a is not None: 
                const.action_a = new_act_a
            if new_act_b is not None: 
                const.action_b = new_act_b

    all_constraints_set = set(all_constraints)

    #then, if available, match reference activities from event log
    if target_activity_list != []: #we HAVE activities from reference event log
        #check for all constraints in all_constraints if we can match one of its actions with one of the reference labels -> find the best and check if similarity > 0.8
        for const in all_constraints_set:
            new_act_a = None
            new_act_b = None
            if const.action_a is not None:
                most_similar_action, similarity_value = get_most_similar_activity_without_common_words(const.action_a.action_str(), target_activity_list)
                if similarity_value >= MATCHING_THRESHOLD:
                    new_act_a = Action(most_similar_action, 0)
            if const.action_b is not None:
                most_similar_action, similarity_value = get_most_similar_activity_without_common_words(const.action_b.action_str(), target_activity_list)
                if similarity_value >= MATCHING_THRESHOLD:
                    new_act_b = Action(most_similar_action, 0)
            if new_act_a is not None: 
                const.action_a = new_act_a
            if new_act_b is not None: 
                const.action_b = new_act_b

    return all_constraints_set

def conformance_checking(textstring:str, xes_log:str):
    """
    Conformance_checking: This function takes a text string (`textstring`) and an event log. If the event log has a column "Concept:Name" , the activities 
    from the constraints are matched with those labels. Also the event log is processed into a D4PY model. The found constraints from the 
    D4PY model aren then compared to all found constraints from the textstring when using run_on_text(...)

    Parameters:
        - textstring (str): A text string containing sentences to be processed.
        - event_log (str): An event log for activity matching & for conformance checking by creating a D4PY model

    Returns:
        - CSV file with different conformance metrics
    
    """
    all_constraints_set = create_constraint_set_from_text(textstring, xes_log)
    reference_model = create_d4py_declaremodel(all_constraints_set)
    declare_model = DeclareModel().parse_from_string(reference_model,"\n")
    model_constraints = declare_model.get_decl_model_constraints()
    print("decl_const", model_constraints)

    with tempfile.NamedTemporaryFile(delete=False) as file:
        file.write(str.encode(xes_log))
        file_path = file.name

    event_log = D4PyEventLog(case_name="case:concept:name")
    event_log.parse_xes_log(file_path) 
    discovery = DeclareMiner(log=event_log, consider_vacuity=False, min_support=0.66, itemsets_support=0.66, max_declare_cardinality=2)
    discovered_model: DeclareModel = discovery.run()
    log_constraints = discovered_model.get_decl_model_constraints()
    print("log_const", log_constraints)

    result_csv = get_conformance_metrics(declare_model, event_log)
    precision = compute_precision(model_constraints, log_constraints)
    print(precision)
    result_csv = result_csv + "\n precision:;" + str(precision)

    pathlib.Path(file_path).unlink(missing_ok=True)

    return result_csv

def get_activities_from_event_log(xes_file):
    """
    get_activities_from_event_log: Produces a unique list of activities extracted from an event log.

    Parameters:
        - xes_file (str): A xes file as string.

    Returns:
        - list of unique activities from the column "concept:name"
    """
    activities = set() #use set so the ativities are unique
    # Parse file 
    root = ET.fromstring(xes_file)
    # Define the namespace used in XES files
    ns = {'xes': 'http://www.xes-standard.org/'}

    # Find all activities
    for trace in root.findall(".//xes:trace", namespaces=ns):
        for event in trace.findall(".//xes:event", namespaces=ns):
            act = event.find(".//xes:string[@key='concept:name']", namespaces=ns)
            if act is not None:
                activity = act.attrib.get('value')
                activities.add(activity)

    return list(activities) #return as list

