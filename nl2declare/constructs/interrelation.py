from nl2declare.constructs.action import Action

class Interrelation:
    """
    This class represents interrelations. An interrelation can have one or two actions and a relation type between those. 
    """
    
    def __init__(self, action_a, action_b, relation_type):
        self.action_a = action_a
        self.action_b = action_b
        self.type = relation_type

    def __str__(self):
        return f"Interrelation [A={self.action_a}, B={self.action_b}, type={self.type}]"

    def __hash__(self):
        return hash((self.action_a, self.action_b))

    def __eq__(self, other):
        if not isinstance(other, Interrelation):
            return False
        return self.action_a == other.action_a and self.action_b == other.action_b