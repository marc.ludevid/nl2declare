from decimal import Decimal
from typing import Tuple, Dict

from nl2declare.constructs.action import Action
from nl2declare.constructs.declare_constraint import DeclareConstraint
from nl2declare.constructs.declare_model import DeclareModel


class Evaluator:
    def __init__(self):
        self.df = Decimal('0.00')
        self.tpm = {} #true positives
        self.fpm = {} #false positives 
        self.fnm = {} #false negatives
        self.tem = {} #type errors
        self.aem = {} #activity errors

    def evaluate_case(self, generated: DeclareModel, gs: DeclareModel) -> Tuple[float, float, Dict[DeclareConstraint, DeclareConstraint]]:
        col = gs.collection

        if col not in self.tpm:
            self.tpm[col] = 0
            self.fpm[col] = 0
            self.fnm[col] = 0
            self.tem[col] = 0
            self.aem[col] = 0

        mtp = 0
        mfp = 0
        mfn = 0

        for gs_con in gs.constraints:
            mfn += gs_con.size()

        matches = self.greedy_matching(generated, gs) #create matches dictionary Dict[DeclareConstraint, DeclareConstraint]

        for gen_con, gs_con in matches.items():
            tp = self.quantify_match(gen_con, gs_con)
            fp = gen_con.size() - tp
            fn = 0 if gs_con is None else gs_con.size() - tp

            mtp += tp
            mfp += fp
            mfn -= tp

            if fp != 0 and fn != 0:
                print(f"GS:    {gs_con}")
                if gen_con.type != gs_con.type:
                    self.tem[col] += 1
                    print(f"TYPE ERROR. Found: {gen_con.type}. GSType: {gs_con.type}")
                if not self.equal_actions(gen_con.action_a, gs_con.action_a):
                    self.aem[col] += 1
                if not self.equal_actions(gen_con.action_b, gs_con.action_b):
                    self.aem[col] += 1

        for gs_constraint in gs.constraints:
            if gs_constraint not in matches.values():
                print(f"MISSED: {gs_constraint}")

        prec = 0 if mtp + mfp == 0 else Decimal(mtp) / Decimal(mtp + mfp)
        rec = Decimal(mtp) / Decimal(mtp + mfn)
        print(f"Case precision: {prec:.2f} recall: {rec:.2f}\n")

        self.tpm[col] += mtp
        self.fpm[col] += mfp
        self.fnm[col] += mfn

        return prec, rec, matches

    def print_collection_results(self, col: int) -> None:
        print(f"\nCollection: {col}")
        ttp = self.tpm[col]
        tfp = self.fpm[col]
        tfn = self.fnm[col]

        prec = Decimal(ttp) / Decimal(ttp + tfp)
        rec = Decimal(ttp) / Decimal(ttp + tfn)

        print(f"Type errors: {self.tem[col]}")
        print(f"Activity errors: {self.aem[col]}")
        print(f"Overall precision: {prec:.2f} recall: {rec:.2f}")

    def print_overall_results(self) -> None:
        ttp = 0
        tfp = 0
        tfn = 0
        type_errors = 0
        act_errors = 0

        for collection in self.tpm.keys():
            self.print_collection_results(collection)
            ttp += self.tpm[collection]
            tfp += self.fpm[collection]
            tfn += self.fnm[collection]
            type_errors += self.tem[collection]
            act_errors += self.aem[collection]

        prec = Decimal(ttp) / Decimal(ttp + tfp)
        rec = Decimal(ttp) / Decimal(ttp + tfn)

        print(f"\nType errors: {type_errors}")
        print(f"Activity errors: {act_errors}")
        print(f"Overall precision: {prec:.2f} recall: {rec:.2f}")

    def greedy_matching(self, gen_model: DeclareModel, gs_model: DeclareModel) -> Dict[DeclareConstraint, DeclareConstraint]:
        res = {}

        for score in range(4, -1, -1):
            for gen_con in gen_model.constraints:
                for gs_con in gs_model.constraints:
                    if self.quantify_match(gen_con, gs_con) == score:
                        if gen_con not in res and gs_con not in res.values():
                            res[gen_con] = gs_con

        for gen_con in gen_model.constraints:
            if gen_con not in res:
                res[gen_con] = None

        return res

    def quantify_match(self, con1: DeclareConstraint, con2: DeclareConstraint) -> int:
        if con1 is None or con2 is None:
            return 0

        res = 0
        if con1.type is not None and con1.type == con2.type:
            res += 1
        if self.equal_actions(con1.action_a, con2.action_a):
            res += 1

        if con2.action_b is not None and self.equal_actions(con1.action_b, con2.action_b):
            res += 1

        if con2.is_negative and con1.is_negative:
            res += 1
            if not (con1.type is not None and con1.type == con2.type):
                res += 1

        return res

    @staticmethod
    def equal_actions(action1: Action, action2: Action) -> bool:
        if action1 is None and action2 is None:
            return True
        if action1 is None or action2 is None:
            return False

        gs_verb = action2.action_str().split(" ")[0]

        return action1.action_str().lower().find(gs_verb) != -1 or action2.action_str().find(action1.verb) != -1