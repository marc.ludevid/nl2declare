nl2declare.actmatching package
==============================

Submodules
----------

nl2declare.actmatching.activity\_matching\_bert module
------------------------------------------------------

.. automodule:: nl2declare.actmatching.activity_matching_bert
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.actmatching.activity\_matching\_word2vec module
----------------------------------------------------------

.. automodule:: nl2declare.actmatching.activity_matching_word2vec
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.actmatching
   :members:
   :undoc-members:
   :show-inheritance:
