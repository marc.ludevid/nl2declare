class TextModel:
    """
    This class represents a textmodel. It consists of lists of actions, noun phrases and interrelations. 
    Also the text and a map of the actions is given.
    """
    
    def __init__(self, text):
        self.text = text
        self.noun_phrases = []
        self.actions = []
        self.interrelations = []
        self.actions_map = {}

    def add_action(self, a):
        self.actions.append(a)
        self.actions_map[a.verb_id] = a

    def add_action_id(self, a, index):
        self.actions_map[index] = a
    
    def get_action_by_verb_id(self, index):
        return self.actions_map.get(index)

    def get_noun_phrase_by_index(self, index):
        for np in self.noun_phrases:
            if np.in_span(index):
                return np
        return None

    def add_interrelation(self, rel):
        self.interrelations.append(rel)

    def preceeding_actions(self, a):
        return self.actions[:self.actions.index(a)]

    def succeeding_actions(self, a):
        return self.actions[self.actions.index(a) + 1:]

    def contains_interrelation(self, rel):
        return rel in self.interrelations
    
    def no_of_flow_actions(self):
        counter = 0
        for action in self.actions:
            if(action.is_flow_action):
                counter = counter + 1
        return counter
            
