from nl2declare.utils.io import load_cases

"""
    This class is just for testing.
"""

__EXPECTED_CASES__ = ['A claim should be created, before it can be approved.',
    'A claim must be created, before it is approved.',
    'When a claim is created, it may be approved.',
    'If a claim is created, it can be approved.',
    'They have to create the claim, before they can approve it.',
    'Only after a claim is created, it is possible to approve the claim.',
    'If a claim is approved, then it must have been created first.',
    'To approve a claim, it should be created first.',
    'A claim can be approved, after it has been created.',
    'Approving the claim can happen at anytime, unless it still needs to be created.',
    'Creation of the claim is required, before it can be approved.',
    'The creation of the claim is a condition for the approval of the claim.',
    'Only after the creation of a claim, the approval of the claim is possible.',
    'When approval of the claim occurs, creation of the claim must have preceded.',
    'If approval of the claim occurs, then creation of the claim must have been carried out first.',
    'The approval of the claim requires that the creation of the claim occurs before it.',
    'The approval of the claim should be preceded by the creation of the claim.',
    'A claim must be created, before it can be approved or rejected.',
    'A claim should be approved before it can be paid out.',
    'Once a claim has been approved, it should eventually be paid out.',
    'Once a claim has been rejected, it should not be paid out.',
    'Payout should end the process and therefore exclude everything.',
    'A case can be closed manually at any time, unless we still need to pay out the claim.',
    'The case can be managed at the union after it has been created.',
    'After a case is created, management can and must arrange a meeting between the union case worker and the other case worker.',
    'After a meeting is arranged it must be held.',
    'The case worker should enter metadata on the case, inform about when he is available for participating in a meeting and then submit the case.',
    'When a case is submitted it may get a local id at the union, but it should also subsequently be assigned a case id in LO.',
    'When a case is submitted, LO should eventually propose dates.',
    'Only after LO has assigned its case id it is possible to manage the case and for LO to propose dates.',
    'Once a date has been agreed upon a meeting should eventually be held.',
    'Every process instance starts by examining a patient.',
    'If an x-ray is taken, then the x-ray risk must be checked before it',
    'Performing a reposition, applying a cast and performing surgery require that x-rays are performed before.',
    'If a surgery is performed, then prescription of rehabilitation occurs eventually after it.',
    'After a cast is applied, eventually the cast is removed and vice versa, before every removal of a cast, a cast must be applied.',
    'The process begins with the booking of the ticket',
    'Personal information of passengers can be provided after the booking of the ticket',
    'The payment of the ticket triggers the completion of the booking phase.',
    'The payment of the ticket is eventually followed by the actual transfer of money',
    'As long as the Check-in of the flight does not take place, the customers can still modify the provided data to change',
    'After check-in of the flight, only the cancellation is admitted',
    'The process starts when an invoice is received by the administration department.',
    'If they decide that the invoice appears legit then they enter all relevant data into the system',
    'When all necessary approvals have been received the invoice can be paid.',
    'Once payment is confirmed, the invoice case should be closed.',
    'The receipt of an invoice is required before the administration department can enter data or scan the invoice.',
    'Enter data is required before any approval can be given.',
    'Finally, we have to pay the invoice before we can confirm payment.',
    'If a request is sent, then a proposal is expected to be prepared afterwards',
    'A confirmation is supposed to be mandatorily given after the proposal, and vice-versa any proposal is expected to precede a confirmation ',
    'Every process instance has to start by registering client data',
    'Every provided room service must be billed.',
    'Every provided laundry service must be billed',
    'When the client leaves, the bill must be charged',
    'The Police Report related to the car accident is searched within the Police Report database and put in a file together with the Claim Documentation.',
    'A customer brings in a defective computer and the manager analyzes the defect and gives back an invoice.',
    'After each of these activities, the proper system functionality is tested.',
    'If an error is detected, another arbitrary repair activity is executed, otherwise the repair is finished.',
    'After a claim is registered, it is examined by a claims officer.',
    'This recommendation is then checked by a senior claims officer who may mark the claim as OK or Not OK.',
    'If the claim is marked as Not OK, it is sent back to the claims officer and the recommendation is repeated.',
    'If the claim is OK, the claim handling process proceeds.',
    'The process of buying new hardware is started as soon as a request for new hardware is received by the IT department.',
    'The next step is for the IT department to analyse the request and either approve or deny it.',
    'If it is denied, the employee is informed via email about the denial and the process is ended.',
    'However, if the request is approved it is edited and completed.',
    'Once this offer is received, a form is filled in and sent to the management.',
    'The management then analyses the request and decides if it is approved or not.',
    'When this is accepted and the financial resources are found, the hardware is received by the IT department.',
    'After which the financial department will take care of the invoice and the processes is ended.',
    'In the scenario where the management does not approve the request, it can send the request back to the IT department for rework, then the IT department has to go back to editing and completing a request.', 'If the management does not approve the request, it can also decide to deny the request after which the process is ended.',
    'The loan approval process starts by receiving a customer request for a loan amount.',
    'If the customer needs further review or the loan amount is for \\$10,000 or more, the request is sent to the approver Web service.',
    'The process of an Office Supply Request starts when any employee of the organization submits an office supply request.',
    'Once the requirement is registered, the request is received by the immediate supervisor of the employee requesting the office supplies.',
    'If the request is rejected, the process will end.',
    'If the request is asked to make a change, then it is returned to the petitioner who can review the comments for the change request.',
    'If the request is approved, it will go to the purchase department that will be in charge of making quotations (using a subprocess) and select a vendor.',
    'After a vendor is selected and confirmed, the system will generate and send a purchase order and wait for the product to be delivered and the invoice received.',
    'In any of the cases, approval, rejection or change required the system will send the user a notification.',
    'The process starts when any employee of the organization submits a vacation request.',
    'Once the requirement is registered, the request is received by the immediate supervisor of the employee requesting the vacation.',
    'If the request is rejected, the application is returned to the applicant/employee who can review the rejection reasons.',
    'If the request is approved, a notification is generated to the Human Resources Representative, who must complete the respective management procedures.',
    'The process starts by reviewing the order.', 'If something is missing, the staff has to inform customer about a potential delay and order the missing products.',
    'After waiting an average of x days for the delivery, the staff should check again if all products are on store.',
    'The courier company informs the client about a potential delay until there are enough human resources to process the delivery.',
    'Once the driver delivers the package, she should update the system so all stakeholders are notified.',
    'Whenever the sales department receives an order, a new process instance is created.',
    'If the storehouse has successfully reserved or back-ordered every item of the part list and the preparation activity has finished, the engineering department assembles the bicycle.',
    'Afterwards, the sales department ships the bicycle to the customer and finishes the process instance.',
    'If the requested amount is lower or equal to 1M$, the company assess the risk of the credit.',
    'After the approval request, the credit could be accepted or rejected',
    'If the requested amount is greater than 1M$, an approval must be requested.',
    'If the requested amount is lower or equal to 1M$, the company assess the risk of the credit.',
    'As soon as an employee files an expense report, the process is started.',
    'Once the expense report is received, a check is performed to make sure the employee has an account.',
    'If this check does not take place within 28 days, the process is stopped and the employee is sent a submit email.',
    "First, the supervisor approves within 7 days, this will result in the deposit of the reimbursement directly to the employee's bank account.",
    'Second, the supervisor rejects within 7 days, as a result the employee receives a notification through email that the request has been rejected.',
    'Third, if it takes the supervisor more than 7 days to respond, an email is sent to the employee informing him/her that the approval is in progress and the part of the processes after a supervisor is asked for approval is repeated.'
]

#def test_load_cases():
    # Test normal case:
    #assert load_cases('input/datacollectioncsv') == __EXPECTED_CASES__
# not working because expected cases need to be updated

def test_load_cases_with_missing_file():
    # Error due to missing file:
    assert load_cases('not_a_file.csv') is None

def test_load_cases_with_missing_header():
    # Error due to missing header:
    assert load_cases('test/utils/missing_header.csv') is None

# def test_load_cases_with_missing_field():
#     # Empty due to missing fields in a the only row, but no error:
#     assert load_cases('test/utils/no_content.csv') == []
