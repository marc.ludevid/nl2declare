from Declare4Py.ProcessModels.DeclareModel import DeclareModel
from Declare4Py.ProcessMiningTasks.Discovery.DeclareMiner import DeclareMiner
from Declare4Py.D4PyEventLog import D4PyEventLog
from nl2declare.constructs.declare_constraint import DeclareConstraint
from nl2declare.constructs.constraint_type import ConstraintType
from Declare4Py.ProcessMiningTasks.ConformanceChecking.MPDeclareAnalyzer import MPDeclareAnalyzer
from Declare4Py.ProcessMiningTasks.ConformanceChecking.MPDeclareResultsBrowser import MPDeclareResultsBrowser

"""
    This class uses Declare4Py to create declare models and compute comformance metrics.
"""

def create_d4py_declaremodel (constraints: set[DeclareConstraint]):
    """
    Create_d4py_declaremodel: Transforms Declare constraints into Declare4Py's Declare model format.

    This function takes a set of Declare constraints. First, it collects all activities that occur in these constraints and concatenates their labels.
    Then, it iterates through each constraint and also appends the constraint's string representation to the result string.
    Eventually, the result string represents the content of a .decl file containing the Declare Model.
    

    Parameters:
        - constraints (set[DeclareConstraint]): contains all constraints that should be included in the Declare model

    Returns:
        - result string with content of .decl file

    """
    result = ""
    activities = []
    for const in constraints:
        if const.action_a is not None:
            activities.append(const.action_a.action_str())
        if const.action_b is not None:
            activities.append(const.action_b.action_str())
    
    #remove duplicates
    activities = list(set(activities))

    for act in activities:
        result = result + "activity " + act + "\n"
    
    for const in constraints:
        if const.type == ConstraintType.INIT:
            result = result + "Init[" + const.action_a.action_str() + "] | | " + "\n"
        elif const.type == ConstraintType.END:
            result = result + "End[" + const.action_a.action_str() + "] | | " + "\n"
        elif const.type == ConstraintType.RESPONSE:
            if const.is_negative:
                result = result + "Not Response[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"
            else:
                result = result + "Response[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"    
        elif const.type == ConstraintType.PRECEDENCE:
            if const.is_negative:
                result = result + "Not Precedence[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"
            else:
                result = result + "Precedence[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"
        elif const.type == ConstraintType.SUCCESSION:
            if const.is_negative:
                result = result + "Not Succession[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"
            else:
                result = result + "Succession[" + const.action_a.action_str() + ", " + const.action_b.action_str() + "] | | | " + "\n"
    
    return result

def compute_precision(model_constraints: [str], log_constraints: [str]):
    """
    Compute_precision: Computes precision value by dividing the number of constraints occurring in both model and log by the number of constraints in the model.

    This function takes two lists of strings where the strings correspond to string representations of Declare constraints.
    The elements of the lists are compared pairwise to count the number of matches before returning the precision value.
    

    Parameters:
        - model_constraints ([str]): contains all string representations of constraints of the NLP-discovered model
        - log_constraints ([str]): contains all string representations of constraints of the Declare4Py-discovered model (from event log)

    Returns:
        - precision value between 0 and 1

    """
    matches = 0
    for model_const in model_constraints:
        #remove | from the constraints
        split_parts = model_const.split(']')
        model_const_new = split_parts[0] + ']'
        for log_const in log_constraints:
            #remove | from the constraints
            split_parts = log_const.split(']')
            log_const_new = split_parts[0] + ']'
            if model_const_new == log_const_new:
                matches +=1
                break
    
    return matches/len(model_constraints)

def get_conformance_metrics(model, event_log):
    """
    Get_conformance_metrics: Applies Declare4Py's conformance checking algorithm to create a result csv containing coformance metrics between log and model. 

    This function takes a Declare model and an event log as input and creates the content of a result csv as string using a sub-function.
    The conformance metrics are: no. of activations, violations, fulfillments, and state.
    

    Parameters:
        - model (DeclareModel): Declare model 
        - log (D4PyEventLog): event log

    Returns:
        - string representation of a csv containing the conformance results 

    """
    basic_checker = MPDeclareAnalyzer(log=event_log, declare_model=model, consider_vacuity=False)
    conf_check_res: MPDeclareResultsBrowser = basic_checker.run()

    result_csv = create_result_csv_content(conf_check_res)
    
    return result_csv

def create_result_csv_content(conf_check_res):
    csv_result = ""

    #no. activations
    csv_result = csv_result + "number of activations \n"
    csv_result = csv_result + "trace id"
    df = conf_check_res.get_metric("num_activations")
    sub_result = df.to_csv(sep=';')
    csv_result = csv_result + sub_result

    #no. fulfillments
    csv_result = csv_result + "number of fulfillments \n"
    csv_result = csv_result + "trace id"
    df = conf_check_res.get_metric("num_fulfillments")
    sub_result = df.to_csv(sep=';')
    csv_result = csv_result + sub_result

    #no. violations
    csv_result = csv_result + "number of violations \n"
    csv_result = csv_result + "trace id"
    df = conf_check_res.get_metric("num_violations")
    sub_result = df.to_csv(sep=';')
    csv_result = csv_result + sub_result

    #state
    csv_result = csv_result + "state \n"
    csv_result = csv_result + "trace id"
    df = conf_check_res.get_metric("state")
    sub_result = df.to_csv(sep=';')
    csv_result = csv_result + sub_result

    return csv_result
