from nl2declare.lingproc.text_parser import TextParser
from nl2declare.constructs.interrelation import Interrelation
from nl2declare.constructs.action import Action
from nl2declare.constructs.relation_type import RelationType

"""
    This class is just for testing.
"""

# def test_text_parser_approval(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("The approval of the claim should be preceded by the creation of an invoice.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['The approval of the claim', 'the creation of an invoice']" in captured.out
#     assert "Action:   the approval of the claim" in captured.out
#     assert "Action:   the creation of an invoice" in captured.out
#     assert "Interrelation [A=Action:   the creation of an invoice, B=Action:   the approval of the claim, type=RelationType.FOLLOWS]" in captured.out

# def test_text_parser_process_starts(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("The process starts when an invoice is received by the administration department.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['The process', 'an invoice', 'the administration department']" in captured.out

# def test_text_parser_activity_system(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("After each of these activities, the proper system functionality is tested.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['these activities', 'the proper system functionality']" in captured.out

# def test_text_parser_man_order(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("The man creates an order.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['The man', 'an order']" in captured.out

# def test_text_parser_creation_claim(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("A creation of a claim precedes creation of document.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['A creation of a claim', 'creation of document']" in captured.out

# def test_text_parser_claim_rejected(capsys):
#     text_parser = TextParser()
#     text_parser.parse_constraint_string("Once a claim has been rejected, it should not be paid out.")
#     captured = capsys.readouterr()
#     assert "Noun Phrases: ['it', 'a claim']" in captured.out



#new tests

def test_text_parser_two_mandatory_verbs(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("A claim must be created before an invoice must be sent.")
    all_interrelations = ""
    for i in result.interrelations:
        all_interrelations = all_interrelations + i.__str__()
    assert "Interrelation [A=Action: create claim, B=Action: send invoice, type=RelationType.FOLLOWS]" in all_interrelations 


def test_text_parser_change_direction(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("A claim must be created after an invoice is sent.")
    all_interrelations = ""
    for i in result.interrelations:
        all_interrelations = all_interrelations + i.__str__()
    assert "Interrelation [A=Action: send invoice, B=Action: create claim, type=RelationType.FOLLOWS]" in all_interrelations

def test_text_parser_passive_required_form(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("Sending an invoice is required to create a claim.")
    all_interrelations = ""
    for i in result.interrelations:
        all_interrelations = all_interrelations + i.__str__()
    assert "Interrelation [A=Action: send invoice, B=Action: create claim, type=RelationType.FOLLOWS]" in all_interrelations
 
def test_text_parser_is_preceded(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("Sending an invoice is preceded by creating a claim.")
    all_interrelations = ""
    for i in result.interrelations:
      all_interrelations = all_interrelations + i.__str__()
    assert "Interrelation [A=Action: create claim, B=Action: send invoice, type=RelationType.FOLLOWS]" in all_interrelations

def test_text_parser_proper_noun_flat(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("A claim must be created before an invoice must be sent to Boris Johannes Schmitz.")
    all_interrelations = ""
    for i in result.interrelations:
        all_interrelations = all_interrelations + i.__str__()
    assert "Interrelation [A=Action: create claim, B=Action: send invoice, type=RelationType.FOLLOWS]" in all_interrelations 

def test_text_parser_empty_model(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("Test")
    all_actions = ""
    for i in result.actions:
        all_actions = all_actions + i.__str__()
    assert "Action: test" in all_actions 

def test_text_parser_one_action(capsys):
    text_parser = TextParser()
    result = text_parser.parse_constraint_string("Creating an invoice.")
    all_actions = ""
    for i in result.actions:
        all_actions = all_actions + i.__str__()
    assert "Action: create invoice" in all_actions 



