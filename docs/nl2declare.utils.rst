nl2declare.utils package
========================

Submodules
----------

nl2declare.utils.io module
--------------------------

.. automodule:: nl2declare.utils.io
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.utils.word\_classes module
-------------------------------------

.. automodule:: nl2declare.utils.word_classes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.utils
   :members:
   :undoc-members:
   :show-inheritance:
