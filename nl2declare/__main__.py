import sys
from nl2declare.lingproc.text_parser import TextParser
from nl2declare.lingproc.declare_constructor import DeclareConstructor
from nl2declare.evaluation.evaluator import Evaluator
import nl2declare.utils.io as IO

def run_single_constraint(text: str):
    parser = TextParser()
    declare_constructor = DeclareConstructor()

    print("Parsing case: " + text)
    text_model = parser.parse_constraint_string(text)
    print("Extracted actions: ")
    for action in text_model.actions:
        print(action)
    print("Extracted relations:")
    for rel in text_model.interrelations:
        print(rel)
    
    declare_model = declare_constructor.convert_to_declare_model(text_model)
    declare_model.sentence_model = text_model

    return declare_model

def run_approach_on_csv_file(csv_input_path: str):
    START = 0
    END = 190
    
    parser = TextParser()
    declare_constructor = DeclareConstructor()
    evaluator = Evaluator()
    gen_models = []
    eval_results_list = []

    gs_models, texts = load_cases(csv_input_path)


    for i in range(START, min(len(texts), END + 1)):
        gs_model = gs_models[i]
        text = texts[i]
        print(str(i) + " Parsing case: " + text)
        text_model = parser.parse_constraint_string(text)

        print("Extracted actions:")
        for action in text_model.actions:
            print(action)

        print("Extracted relations:")
        for rel in text_model.interrelations:
            print(rel)

        declare_model = declare_constructor.convert_to_declare_model(text_model)
        declare_model.sentence_model = text_model

        gen_models.append(declare_model)

        eval_res = evaluator.evaluate_case(declare_model, gs_model)
        eval_results_list.append(eval_res)

    evaluator.print_overall_results()
    return gen_models

def load_cases(csv_input_path: str):
    gs_models = IO.load_cases(csv_input_path)
    if gs_models is None:
        return None
    texts = []
    for model in gs_models:
        texts.append(model.text)
    return gs_models, texts

if __name__ == '__main__':
    
    CONSTRAINT_FILE = "nl2declare/input/datacollection.csv"
    RESULTS_FILE = "nl2declare/output/results.csv"
    
    args = sys.argv
    if len(args) == 1:
        run_approach_on_csv_file(CONSTRAINT_FILE)
    else:
        run_single_constraint(args[1])
    
    
   