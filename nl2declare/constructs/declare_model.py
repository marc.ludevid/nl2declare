class DeclareModel:
    """
    This class represents a declare model. A model has a text and a list of constraints deduced from this text. 
    """

    def __init__(self, text, collection=None, ID=None):
        self.text = text
        self.sentence_model = None
        self.collection = collection #for evaluation
        self.ID = ID #for evaluation
        self.constraints = []

    def get_size(self):
        return sum(constraint.size() for constraint in self.constraints)