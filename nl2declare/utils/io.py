from __future__ import annotations
import csv
import os
from typing import Optional
from nl2declare.constructs.declare_constraint import DeclareConstraint
from nl2declare.constructs.declare_model import DeclareModel
from nl2declare.constructs.constraint_type import ConstraintType
from nl2declare.constructs.action import Action

"""
    This class enables the loading of CSV files.
"""

__HEADER__ = ['collection', 'Source', 'original', 'reasons for modication', 'ID',
    'Used constraint', 'Constraints', 'Negative', 'Type', 'ActionA', 'ActionB', 'Type',
    'ActionA', 'ActionB', 'Type', 'ActionA', 'ActionB', '', '', '', '', '', '', '', '', '']

def load_csv(csv_path: str) -> Optional[list[list[str]]]:
    absolute_path = os.path.abspath(csv_path)
    if not os.path.isfile(absolute_path):
        print(absolute_path + ' is not a file.')
        return None

    print('Reading input file: ' + absolute_path)
    with open(absolute_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        header = next(csv_reader)
        if (not header == __HEADER__):
            print('Unexpected header in csv file.')
            return None
        return [row for row in csv_reader]

def load_cases(csv_path: str) -> Optional[list[DeclareModel]]:
    csv = load_csv(csv_path)
    if csv is None:
        return None
    cases = []
    for row in csv:
        if (len(row) == 0):
            continue
        declare_model = DeclareModel(row[5], int(row[0]), int(row[4]))
        num_constraints = int(row[6])
        is_negative = row[7].casefold() == "true".casefold()
        for i in range(num_constraints):
            type = ConstraintType.get_type(row[8 + i * 3])
            action_a = Action(None, None, row[9 + i * 3])
            if row[10 + i * 3] is None or row[10 + i * 3] == "":
                gsc = DeclareConstraint(type, action_a)
            else:
                action_b = Action(None, None, row[10 + i * 3])
                gsc = DeclareConstraint(type, action_a, action_b)
            if is_negative:
                gsc.is_negative = True
            declare_model.constraints.append(gsc)
        cases.append(declare_model)
    return cases

