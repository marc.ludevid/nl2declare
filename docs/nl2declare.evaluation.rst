nl2declare.evaluation package
=============================

Submodules
----------

nl2declare.evaluation.evaluator module
--------------------------------------

.. automodule:: nl2declare.evaluation.evaluator
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.evaluation
   :members:
   :undoc-members:
   :show-inheritance:
