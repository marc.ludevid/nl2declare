# NL2DECLARE

## How to run the project

You can use the nl2declare project in two different ways: using the python library or using the provided GUI.

- **nl2declare** python library: simply include the python library in you python project and start using it. Example:

```python
import nl2declare

parser = TextParser()
declare_constructor = DeclareConstructor()
text_model_for_sentence = parser.parse_constraint_string(sentence)
declare_model_for_sentence = declare_constructor.convert_to_declare_model(text_model_for_sentence)
declare_model_for_sentence.sentence_model = text_model_for_sentence
```

- **GUI**: You can also use the provided GUI. Use the provded docker files to build the service and host it on your machine with:

```bash
$ docker-compose build
$ docker-compose up
```

Then you can access the GUI on your webbrowser with with the following URL: `http://localhost:8888`