# NL2Declare

## Testing:
- Testing can be done with `pytest`. The repository contains a `pytest.ini` file that sets the following options: `--cov=nl2declare --cov-report=xml:cov.xml --cov-report=term`. We always check code coverage and report it as xml to facilitate usage of coverage plugins in the IDE.
- If you want to programatically check if coverage was below a certain threshold run `coverage report --fail-under=<threshold>`.