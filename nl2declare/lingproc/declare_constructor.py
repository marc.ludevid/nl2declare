from nl2declare.constructs.declare_model import DeclareModel
from nl2declare.constructs.relation_type import RelationType
from nl2declare.constructs.declare_constraint import DeclareConstraint
from nl2declare.constructs.constraint_type import ConstraintType
from nl2declare.utils.word_classes import WordClasses

class DeclareConstructor:
    """
    This class converts a textmodel into a declare model. 
    """

    def convert_to_declare_model(self, model):
        """
        convert_to_declare_model: Converts a given text model into a declare model by converting all interrelations
        of the textmodel into declare constraints.
        
        Parameters:
            - model (TextModel): A textmodel object 

        Returns:
            - Declare model object
        """
        declareModel = DeclareModel(model.text)
        isMeta = False
        wordclasses = WordClasses()
        for rel in model.interrelations:
            #if one action is meta (=includes a process object)
            if self.is_meta_action(rel.action_a) or self.is_meta_action(rel.action_b):
                #Unary start constraints
                if wordclasses.is_start_verb(rel.action_a.verb):
                    ct = ConstraintType.INIT
                    c = DeclareConstraint(ct, rel.action_b)
                    declareModel.constraints.append(c)
                    print("FOUND:", c)
                    isMeta = True
                if wordclasses.is_start_verb(rel.action_b.verb):
                    ct = ConstraintType.INIT
                    c = DeclareConstraint(ct, rel.action_a)
                    declareModel.constraints.append(c)
                    print("FOUND:", c)
                    isMeta = True
                #Unary end constraints
                if wordclasses.is_end_verb(rel.action_a.verb):
                    ct = ConstraintType.END
                    c = DeclareConstraint(ct, rel.action_b)
                    declareModel.constraints.append(c)
                    print("FOUND:", c)
                    isMeta = True
                if wordclasses.is_end_verb(rel.action_b.verb):
                    ct = ConstraintType.END
                    c = DeclareConstraint(ct, rel.action_a)
                    declareModel.constraints.append(c)
                    print("FOUND:", c)
                    isMeta = True

            if isMeta==False and rel.type == RelationType.FOLLOWS:
                ct = None
                aMand = wordclasses.is_mandatory(rel.action_a.modal)
                bMand = wordclasses.is_mandatory(rel.action_b.modal)

                if bMand == False:
                    ct = ConstraintType.PRECEDENCE
                if aMand and bMand:
                    ct = ConstraintType.SUCCESSION
                if bMand and aMand == False:
                    ct = ConstraintType.RESPONSE

                c = DeclareConstraint(ct,rel.action_a, rel.action_b)
                if c.action_a.is_negative or c.action_b.is_negative:
                    c.is_negative = True
                declareModel.constraints.append(c)
        
        return declareModel
        

    def is_meta_action(self, action):
        """
        is_meta_action: Tests if the text of the given action includes a process object (defined in the class word_classes).
        
        Parameters:
            - action (Action): A Action object

        Returns:
            - Boolean value. True = a process object is included
        """
        joint = ""
        if(action.object is not None and action.subject is not None):
            joint = action.object.text + " " + action.subject.text
        elif(action.object is not None):
            joint = action.object.text
        elif(action.subject is not None):
            joint = action.subject.text

        wordclasses = WordClasses()

        for processObject in wordclasses.PROCESS_OBJECTS:
            if processObject in joint:
                return True

        return False
