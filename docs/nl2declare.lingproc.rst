nl2declare.lingproc package
===========================

Submodules
----------

nl2declare.lingproc.declare\_constructor module
-----------------------------------------------

.. automodule:: nl2declare.lingproc.declare_constructor
   :members:
   :undoc-members:
   :show-inheritance:

nl2declare.lingproc.text\_parser module
---------------------------------------

.. automodule:: nl2declare.lingproc.text_parser
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nl2declare.lingproc
   :members:
   :undoc-members:
   :show-inheritance:
