
class Action:

    """
    This class represents actions. Actions are constructed by verbs and can hava an object and a subject. Also
    it is stated wether the action consists of a flow verb, is negative, is passive...
    """

    def __init__(self, verb, verb_id, gstext=None):
        self.modal = ""
        self.verb = verb
        self.verb_id = verb_id
        self.object = None 
        self.subject = None
        self.marker = None
        self.clause = None
        self.is_passive = False
        self.gstext = gstext
        self.conjunctions = set()
        self.past_participle = False
        self.is_negative = False
        self.is_flow_action = False

    def set_verb(self, verb, verb_id):
        self.verb = verb
        self.verb_id = verb_id

    def action_str(self):
        if self.gstext is not None:
            return self.gstext

        sb = []
        sb.append(f"{self.verb}")

        if self.object is not None:
            sb.append(str(self.object))

        return " ".join(sb).lower()

    def __str__(self):
            return f"Action: {self.action_str()}"


    
    def __hash__(self):
        prime = 31
        result = 1
        result = prime * result + (0 if self.object is None else hash(self.object))
        result = prime * result + (0 if self.verb is None else hash(self.verb))
        result = prime * result + self.verb_id
        return result

    def __eq__(self, other):
        if self is other:
            return True
        if other is None or type(self) != type(other):
            return False
        return (
            (self.object is None and other.object is None) or self.object == other.object
        ) and (
            (self.verb is None and other.verb is None) or self.verb == other.verb
        ) and self.verb_id == other.verb_id
